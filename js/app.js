$(document).ready(function(){
  let showNavbar = false
  $('#navbar-toggle').on('click', function(){
    showNavbar = !showNavbar

    if(showNavbar)
      $('#' + $(this).data('target')).addClass('is-visible')
    else
      $('#' + $(this).data('target')).removeClass('is-visible')
  })
})