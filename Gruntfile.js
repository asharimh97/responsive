module.exports = function(grunt){
  grunt.initConfig({
    sass: {
      options: {
        sourceMap: true
      },
      dist: {
          files: {   
            'css/style.css': 'scss/style.scss',
            'css/mobile.css': 'scss/mobile.scss'
          }
      }
    },
    watch: {
      scss: {
        files: ['scss/*.scss'],
        tasks: ['sass']
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default', ['watch']);
  require('load-grunt-tasks')(grunt);
}